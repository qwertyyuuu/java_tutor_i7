package part_1.lesson_4;

public class Solution {
    public static void main(String[] args) {


        System.out.println(checkUpperChar("Asd ASD Jasd 7"));
/*
        StringBuilder s1 = new StringBuilder("     bce ced abc    ");

        final String replace = s1.toString();
//        String[] sortedArray = sortWords(s1);
        System.out.println(replace);
        for (int i = 0; i < 100000; i++) {
            s1.append("as").append("asd").append("asd");
        }
*/

//        System.out.println("a: " + (int)'a' + "\n" + "z: " + (int)'z' );
//        String s2 = "def"; // abcdef
//        s1 = s1 + s2;
//        String s3 = s1.toUpperCase();
//        System.out.println(s3);

/*        String s = "asd:asd:asd";
        String[] arr = s.split(":");*/

//        System.out.println(Arrays.toString(arr));
//        System.out.println(s.substring(3,8));
    }

    public static boolean checkWord(String s) {
        for (int i = 0; i < s.length() / 2; i++) {
            if (s.charAt(i) != s.charAt(s.length() - i - 1)) {
                return false;
            }
        }
        return true;
    }

    public static boolean checkDublicates(String s){
        boolean[] check = new boolean[(int)'z'+1];
        if (s.length() > 28) return false;
        for (int i = 0; i < s.length(); i++) {
            int index = s.charAt(i);
            if (!check[index]){
                check[index] = true;
            }else{
                return false;
            }
        }
        return true;
    }

    public static boolean checkDublicatesWithoutAdditionalData(String s){
        final char[] chars = s.toCharArray();
        bubbleSort(chars);
        if (chars.length > 28) return false;
        for (int i = 0; i < chars.length-1; i++) {
            if (chars[i] == chars[i+1]) return false;
        }
        return true;
    }


    public static void bubbleSort(char[] arr) {

        boolean isSorted = false;
        while (!isSorted) {
            isSorted = true;

            for (int i = 0; i < arr.length - 1; i++) {
                if (arr[i] > arr[i + 1]) {
                    char temp = arr[i];
                    arr[i] = arr[i + 1];
                    arr[i + 1] = temp;
                    isSorted = false;
                }
            }
        }
    }

    public static String[] sortWords(String s ) {
        String[] s1 = s.split(" ");

        boolean isSorted = false;
        while (!isSorted) {
            isSorted = true;

            for (int i = 0; i < s1.length - 1; i++) {
                if (s1[i].compareTo(s1[i+1]) > 0) {
                    String temp = s1[i];
                    s1[i] = s1[i + 1];
                    s1[i + 1] = temp;
                    isSorted = false;
                }
            }
        }
        return s1;
    }


    // ab7e777abce
    // 777
    public static boolean checkTwoString(String src, String target){

        if (target.length() > src.length()) return false;

        for (int i = 0; i < src.length()-target.length(); i++) {
            boolean flag = true;
            for (int j = 0; j < target.length(); j++) {
                if (src.charAt(i+j) != target.charAt(j)) {
                    flag = false;
                    break;
                }
            }
            if (flag) return flag;
        }
        return false;
    }

    public static boolean checkUpperChar(String s){
        String[] s1 = s.split(" ");
        for (String value : s1) {
            if (!(value.charAt(0) >= 'A' && value.charAt(0) <= 'Z')) {
                return false;
            }
        }
        return true;
    }
}
