package part_1.lesson_2;

import java.util.Scanner;

public class Solution {
    public static void main(String[] args) {
/*
        Знакомство с синтаксисом

        int[] arr;
        arr = new int[10];

        // i:    0  1  2  3  4
        // elem   1, 2, 5, 3, 0
        int[] array = new int[10];
        int item = array[3];
        System.out.println(item);

        for (int i = 0; i < 10; i++) {
            System.out.println(array[i]);
        }

        int[] array2 = {1,2,5,3,0};
        int[] array3 = {2,10,12,3,9};

        array3 = array2;
        array3[2] = -1;
        for (int i = 0; i < array2.length; i++) {
            System.out.println(array2[i]);
        }

        int length = array2.length; // длина
        int a = array2[5];          // элемент по индексу 5

        System.out.println(length);
        System.out.println(a);

        boolean[] bools = new boolean[10];
        for (int i = 0; i < bools.length; i++) {
            System.out.println(bools[i]);
        }
*/

       //   Практика

        /*
            1.
            Считать размер массива целых чисел, заполнить его значениями с клавиатуры
            Вывести:
            * построчно все элементы
            * максимальный и минимальный элементы
            * среднее арифметическое элементов
            * элемент, встречающийся чаще остальных (гарантируется, что такой элемент 1)
            * количество элементов, меньше х
        */

        Scanner sc = new Scanner(System.in);
/*        int n = sc.nextInt();
        int[] arr = new int[n];
        for (int i = 0; i < n; i++) {
            arr[i] = sc.nextInt();
        }

        for (int i = 0; i < n; i++) {
            System.out.println(arr[i]);
        }

        int max = Integer.MIN_VALUE;
        int min = Integer.MAX_VALUE;
        int sum = 0;
        for (int i = 0; i < n; i++) {
            if (arr[i] > max) max = arr[i];
            if (arr[i] < min) min = arr[i];
            sum += arr[i];
        }


        int[] cntArray = new int[max+1]; // arr = {1, 2, 5, 5, 3, 3, 9, 9} -> 5
                                        // i       0  1  2  3  4  5
                                        // cnt    {0, 1, 1, 1, 0, 2}
        for (int i = 0; i < n; i++) {
            cntArray[arr[i]]++;
        }


        int maxCnt = cntArray[0];
        int index = 0;
        for (int i = 0; i < cntArray.length; i++) {
            if (cntArray[i]>maxCnt) {
                maxCnt = cntArray[i];
                index = i;
            }
        }

        int x = sc.nextInt();
        int cnt = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i]<x) cnt++;
        }
        System.out.println("MAX: " + max);
        System.out.println("MIN: " + min);
        System.out.println("AVERAGE: " + sum / n);
        System.out.println("MODE: " + index);
        System.out.println("CNT: " + cnt);
        */


        /* 2.
         * Разворот массива (считали 1, 2, 3, превратили в массив 3,2,1 и вывели)
         * Разворот не используя второй массив (с постоянным кол-вом дополнительной памяти)
         * Считать массив, переместить все 0 (если они есть) в начало
         */

        // считали размер
        int n = sc.nextInt(); // n = 5 | i: 0 1 2 3 4

/*      int[] arr = new int[n];
        int[] arrReverse = new int[n];
        for (int i = 0; i < n; i++) {
            arr[i] = sc.nextInt();
        }

        // c использованием дополнительного массива
        for (int i = 0; i < n; i++){
            arrReverse[i] = arr[arr.length-i-1];
        }

        for (int i = 0; i < n; i++) {
            System.out.println(arrReverse[i]);
        }

        // всё в одном массиве
        for (int i = 0; i < n/2; i++) {
            int temp = arr[i];
            arr[i] =  arr[arr.length-i-1];
            arr[arr.length-i-1] = temp;
        }

/*
        for (int i = 0; i < n; i++) {
            System.out.println(arr[i]);
        }*/

        // цикл foreach
/*        for(int elem:arr){
            System.out.println(elem);
        }
*/

      /*  Двумерный массив
      int[][] arr2 = new int[n][n];

       for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                arr2[i][j] = sc.nextInt();
            }
        }

        int middle = arr2[1][1];
        System.out.println(middle);*/


        // [1,2,3]
        /*
           0 [] -> [1,2,3]
           1 [] -> [4,5,6]
           2 [] -> [7,8,9]
        * */
    }
}
