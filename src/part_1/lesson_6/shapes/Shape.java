package part_1.lesson_6.shapes;

public abstract class Shape {
    public abstract double getSquare();
}
