package part_1.lesson_3;

import java.util.Arrays;

public class Solution {

    public static void main(String[] args) {
        // arr - массив который содержит элементы [0,1] {0,1,0,0,1} - > {0, 0, 0, 1, 1}
        int[] arr = {1, 0, 0, 2, 5, 2};
        countingSort(arr, 5);
        System.out.println(Arrays.toString(arr));
    }

    // 5
    // {1, 2, 4,  5 , 9,10}


    public static void countingSort(int[] arr, int p) {
        int[] cnt = new int[p + 1];

        for (int j : arr) {
            cnt[j]++;
        }
        // arr {1, 0, 0, 2, 5, 2}

        // cnt[]
        // i: 0 1 2 3 4 5
        // e: 2 1 2 0 0 1
        int k = 0;
        for (int i = 0; i < p; i++) {
            for (int j = 0; j < cnt[i]; j++) {
                arr[k] = i;
                k++;
            }
        } // 0 0 1
    }

    public static void selectionSort(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            swap(arr, i, getMin(arr, i));
        }
    }


    public static int getMin(int[] arr, int left) {
        int index = left;
        int min = arr[left];

        for (int i = left; i < arr.length; i++) {
            if (arr[i] < min) {
                min = arr[i];
                index = i;
            }
        }
        return index;
    }


    public static void swap(int[] arr, int a, int b) {
        int temp = arr[a];
        arr[a] = arr[b];
        arr[b] = temp;
    }


    public static void bubbleSort(int[] arr) {

        boolean isSorted = false;
        while (!isSorted) {
            isSorted = true;

            for (int i = 0; i < arr.length - 1; i++) {
                if (arr[i] > arr[i + 1]) {
                    int temp = arr[i];
                    arr[i] = arr[i + 1];
                    arr[i + 1] = temp;
                    isSorted = false;
                }
            }
        }
    }


    public static int binarySearch(int[] arr, int target) {
        int left = 0;
        int right = arr.length - 1;

        while (left <= right) {
            int middle = (left + right) / 2;
            if (arr[middle] == target) return middle;

            if (arr[middle] > target) {
                right = middle - 1;
            }
            if (arr[middle] < target) {
                left = middle + 1;
            }
        }
        return -1;
    }


    public static int search(int[] arr, int target) {
        int index = -1;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == target) index = i;
        }
        return index;
    }


/*   public static int inc(int x, int y){
        return x+y;
   }

   public static void print(){
       System.out.println("hi");
   }*/
}

