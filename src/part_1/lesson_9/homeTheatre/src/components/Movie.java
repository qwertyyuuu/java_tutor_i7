package part_1.lesson_9.homeTheatre.src.components;

public abstract class Movie {
    private String name;
    private String genre;
    private int year;


    public Movie() {}

    public Movie(String name) {
        this.name = name;
    }

    public Movie(String name, String genre, int year) {
        this.name = name;
        this.genre = genre;
        this.year = year;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }
}
