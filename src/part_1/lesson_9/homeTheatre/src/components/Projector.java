package part_1.lesson_9.homeTheatre.src.components;


public class Projector {
    private SimplePlayerImpl simplePlayerImpl;

    public void on() {
        System.out.println("Top-O-Line projector on");
    }

    public void off() {
        System.out.println("Top-O-Line projector on");
    }

    public void tvMode() {
        System.out.println("Top-O-Line projector in tv mode");
    }

    public void wideScreenMode() {
        System.out.println("Top-O-Line projector in widescreen mode (16x9 aspect ration)");
    }
}
