package part_1.lesson_9.homeTheatre.src.components;

public class StarWarsRevengeOfTheSithMovie extends Movie{

    private double rate;

    public StarWarsRevengeOfTheSithMovie(String name, String genre, int year, double rate) {
        super(name,genre,year);
        this.rate = rate;
    }

}
