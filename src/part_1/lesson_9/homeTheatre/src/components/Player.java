package part_1.lesson_9.homeTheatre.src.components;

public interface Player {
    void on();
    void off();
    void eject();
    void pause();
    void play(Movie movie);
    void stop();

}
