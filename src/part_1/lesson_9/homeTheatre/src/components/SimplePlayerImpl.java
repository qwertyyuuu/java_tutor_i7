package part_1.lesson_9.homeTheatre.src.components;


public class SimplePlayerImpl implements Player{

    private Movie movie;
    private String model;
    private int yearOfProduce;
    private int volume;

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getYearOfProduce() {
        return yearOfProduce;
    }

    public void setYearOfProduce(int yearOfProduce) {
        this.yearOfProduce = yearOfProduce;
    }

    public int getVolume() {
        return volume;
    }

    public void setVolume(int volume) {
        this.volume = volume;
    }

    public void on() {
        System.out.println("Simple Player on");
    }

    public void off() {
        System.out.println("Simple Player off");
    }

    public void eject() {
        System.out.println("Simple Player eject");
    }

    public void pause() {
        System.out.println("Simple Player pause");
    }

    public void play(Movie movie) {
        this.movie = movie;
        System.out.println("Top-O-Line DVD Player playng: " + movie);
    }

    public void stop() {
        System.out.println("Top-O-Line DVD Player stopped: " + movie);
    }

    @Override
    public String toString() {
        return "Top-O-Line DVD Player";
    }
}
