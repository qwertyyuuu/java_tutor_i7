package part_1.lesson_9.homeTheatre.src;

import part_1.lesson_9.homeTheatre.src.components.*;


public class HomeTheatreTestDrive
{
    public static void main(String[] args)
    {

        Screen screen = new Screen();
        PopcornPopper popper = new PopcornPopper();
        TheaterLights lights = new TheaterLights();
        Player simplePlayerImpl = new SimplePlayerImpl();
        Projector projector = new Projector();

        HomeTheatreFacade homeTheatreFacade = new HomeTheatreFacade(simplePlayerImpl,projector, lights, screen, popper);

        homeTheatreFacade.watchMovie(new StarWarsRevengeOfTheSithMovie(
                "Star wars revenge of the sith",
                "action",
                2005,
                8.1));

        homeTheatreFacade.endMovie();
    }
}
