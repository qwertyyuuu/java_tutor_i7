package part_1.lesson_8;

public class Child extends Parent{
    int a;
    static int b;
    static {
        b = 10;
        System.out.println("static init block - child");
    }

    {
        a = 20;
        System.out.println("non static init block - child");
    }

    public Child(){
//        super();
        // start non static init block
        System.out.println("constructor child");
    }
/*

    public static void bar(){
        System.out.println("child");
    }
*/

    public void blabla(){}

    @Override
    public void saySomething() {
        System.out.println("I'm child object");
    }
}
