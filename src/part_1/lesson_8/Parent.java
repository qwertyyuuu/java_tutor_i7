package part_1.lesson_8;

public class Parent {

    static {
        System.out.println("static init block - parent");
    }

    {
        System.out.println("non static init block - parent");
    }
    public Parent(){
//        super();
        // non static init block
        System.out.println("parent constructor");
    }

    public static void bar(){
        System.out.println("parent");
    }

    public void saySomething(){
        System.out.println("I'm parent object");
    }
}
