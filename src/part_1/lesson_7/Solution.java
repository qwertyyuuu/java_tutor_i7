package part_1.lesson_7;

public class Solution {
    public static void main(String[] args) {
        RemoteControl remoteControl = new RemoteControl();

//        IkPortCompatible tv = new Tv();
        IkPortCompatible conditioner = new Conditioner();

//        Tv3d tv3d = new Tv3d();

        One one = new Tv3d();
        one.a();
        Two two = new Tv3d();
        two.a();
//        Tv tv3d1 = new Tv3d();



//        System.out.println(remoteControl.turnSomethingOn(tv));
        System.out.println(remoteControl.turnSomethingOn(conditioner));
    }

}
