package part_2.lesson_1;

public abstract class TextInput {
    protected String text;

    public String getText() {
        return text;
    }

    public void input(String text){
        this.text = text;
        onInput();
    }

    public abstract void onInput();

}
