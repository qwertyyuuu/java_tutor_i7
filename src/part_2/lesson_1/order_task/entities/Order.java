package part_2.lesson_1.order_task.entities;

import java.util.ArrayList;
import java.util.List;

public class Order {

	private Long id;

	//Размер скидки
	private Integer discountPercent;

	//Позиции заказа
	private List<Item> items;

	private Status status;

	public Order(Long id, Integer discountPercent, List<Item> items, Status status) {
		this.id = id;
		this.discountPercent = discountPercent;
		this.items = items;
		this.status = status;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getDiscountPercent() {
		return discountPercent;
	}

	public void setDiscountPercent(Integer discountPercent) {
		this.discountPercent = discountPercent;
	}

	public List<Item> getItems() {
		return items;
	}

	public void setItems(List<Item> items) {
		this.items = items;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}
	public Order getOrder(){
		return this;
	}

	public Double getTotalPrice(){
		Double total = 0.0;
		for (Item item: items){
			for (int i = 0; i < item.getQuantity(); i++) {
				total+=item.getPrice();
			}
		}
		return total-(total*discountPercent/100);
	}


}
