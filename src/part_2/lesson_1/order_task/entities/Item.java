package part_2.lesson_1.order_task.entities;

import java.util.Objects;

public class Item {

	private final Long id;

	//Число товаров данного вида в заказе
	private Integer quantity;

	private Double price;

	private String name;

	private Boolean isDeleted;


	public Long getId() {
		return id;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public Double getPrice() {
		return price;
	}

	public String getName() {
		return name;
	}

	public Boolean getDeleted() {
		return isDeleted;
	}

	public Item(Long id, Integer quantity, Double price, String name) {
		this.id = id;
		this.price = price;
		this.quantity = quantity;
		this.name = name;
		this.isDeleted = false;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Item item = (Item) o;
		return Objects.equals(quantity, item.quantity) && Objects.equals(price, item.price) && Objects.equals(name, item.name) && Objects.equals(isDeleted, item.isDeleted);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, quantity, price, name, isDeleted);
	}
}
