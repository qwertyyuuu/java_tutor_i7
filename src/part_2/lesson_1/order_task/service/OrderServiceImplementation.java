package part_2.lesson_1.order_task.service;


import part_2.lesson_1.order_task.entities.Item;
import part_2.lesson_1.order_task.entities.Order;
import part_2.lesson_1.order_task.entities.Status;
import part_2.lesson_1.order_task.entities.StatusGroup;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

//Вы проектируете систему для управления заказами интернет магазина. Вам необходимо реализовать интерфейс OrderService
public class OrderServiceImplementation implements OrderService {
    @Override
    public Order getOrderById(List<Order> orders, Long id) {
        return null;
    }

    @Override
    public List<Order> getAllOrdersByStatus(List<Order> orders, Status status) {
        return null;
    }

    @Override
    public List<Order> getAllOrdersByStatusGroup(List<Order> orders, StatusGroup statusGroup) {
        return null;
    }

    @Override
    public List<Order> getAllOrdersWithoutDeletedItems(List<Order> orders) {
        return null;
    }

    @Override
    public List<Order> getOrdersWithPriceGreaterThan(List<Order> orders, Double price) {
        return null;
    }

    @Override
    public Map<Long, Order> idAndOrderMap(Stream<Order> orderStream) {
        return null;
    }

    @Override
    public Double calculateTheSum(List<Order> orders) {
        return null;
    }

    @Override
    public Map<Item, List<Order>> itemAndListOfOrders(List<Order> orderList) {
        return null;
    }
}
