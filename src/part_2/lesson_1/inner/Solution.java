package part_2.lesson_1.inner;

public class Solution {

    public static void main(String[] args) {
//         Table.TableEntry entry = new Table.TableEntry(1, "Эльнур");
        Table.TableEntry[] entries = new Table.TableEntry[]{
                new Table.TableEntry(1, "Elnur1"),
                new Table.TableEntry(2, "Elnur2"),
                new Table.TableEntry(3, "Elnur3"),
        };

        Table table = new Table(entries);
        Table.TableIterator iterator = table.new TableIterator();


        while (iterator.hasNext()){
            System.out.println(iterator.next());
        }
    }

}
